//
//  ASUtility.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation
import UIKit

/// This class will represent some common functions which will be used in the project.
class ASUtility: NSObject {
    
    /// Shared instance
    static let shared = ASUtility()
    //MARK:- TRANSITIONS
    
    /// Get Transitions
    /// - Usage: self.navigationController?.view.layer.add(transition, forKey: kCATransition)
    /// - Parameter duration: CFTimeInterval = 0.5
    /// - Parameter timingFunction: CAMediaTimingFunctionName = .easeInEaseOut
    /// - Parameter type: CATransitionType = .push
    /// - Parameter subType: CATransitionSubtype = .fromTop
    func getTransition(duration: CFTimeInterval = 0.5,
                       timingFunction: CAMediaTimingFunctionName = .easeInEaseOut,
                       type: CATransitionType = .push,
                       subType: CATransitionSubtype = .fromTop ) -> CATransition {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: timingFunction)
        transition.type = type
        transition.subtype = subType
        return transition
    }
    
    /// Print all available fonts in console
    func logAllAvailableFonts() {
        let familyNames = ["FugazOne-Regular","HelveticaNeue"]
        
        for family in familyNames {
            print("Family name " + family)
            let fontNames = UIFont.fontNames(forFamilyName: family)
            
            for font in fontNames {
                print("    Font name: " + font)
            }
        }
    }
    //MARK:- ALERTS
    /// This function is used to show an alert popup for confirmation of user.
    ///
    /// - Parameters:
    ///   - title: Title of the alert
    ///   - message: Brief message for the alert
    ///   - lblDone: Lable string for the done button
    ///   - lblCancel: lable string for the cancel button
    ///   - controller: controller from where this alert is called
    ///   - completion: return the result.
    @objc func showConfirmAlert(with title: String, message: String, lblDone: String, lblCancel: String, completion: @escaping (_ choice: Bool) -> Void) {
        
        let objAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        objAlert.addAction(UIAlertAction(title: lblDone, style: .default, handler: { (_) in
            completion(true)
            return
        }))
        
        objAlert.addAction(UIAlertAction(title: lblCancel, style: .destructive, handler: { (_) in
            completion(false)
            return
        }))
        
        kMainQueue.async {
            guard let topController = UIApplication.topViewController() else {
                kAppDelegate.window?.rootViewController?.present(objAlert, animated: true, completion: nil)
                return
            }
            if topController.isKind(of: UIAlertController.self) {
                print("Not showing alert as another UIAlertController is present already.")
                return
            }
            topController.present(objAlert, animated: true, completion: nil)
        }
    }
    
    /// This function is used to show a dismiss alert popup for user.
    ///
    /// - Parameters:
    ///   - title: Title of the alert
    ///   - message: Brief message for the alert
    ///   - lblDone: Lable string for the done button
    ///   - controller: controller from where this alert is called
    ///   - onPrevAlert: if true then the alert will be shown on previous alert itself if present. else only present alert if there is no alert controller already showing.
    ///   - completion: completion handler once user pressed the button.
    @objc func dissmissAlert(title: String, message: String, lblDone: String, onPrevAlert: Bool = false, completion: ((_ choice: Bool) -> Void)? = nil) {
        
        let objAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        objAlert.addAction(UIAlertAction(title: lblDone, style: .default, handler: { (_) in
            completion?(true)
            return
        }))
        
        kMainQueue.async {
            guard let topController = UIApplication.topViewController() else {
                kAppDelegate.window?.rootViewController?.present(objAlert, animated: true, completion: nil)
                return
            }
            if topController.isKind(of: UIAlertController.self) && onPrevAlert == false {
                print("Not showing alert as another UIAlertController is present already.")
                return
            }
            topController.present(objAlert, animated: true, completion: nil)
        }
    }
    
    
    
    /// Show Simple Alert
    /// - Parameters:
    ///   - title: Title of the alert
    ///   - message: Brief message for the alert
    ///   - completion: Return(true)
    @objc func showAlert(title: String?, message: String?, completion: @escaping (_ choice: Bool) -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: L10n.okay.string, style: UIAlertAction.Style.default, handler: { (_) in
            completion(true)
        }))
        alert.view.tintColor = Color.sideMenuSelectedTitleColor
        
        kMainQueue.async {
            guard let topController = UIApplication.topViewController() else {
                kAppDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
                return
            }
            if topController.isKind(of: UIAlertController.self) {
                print("Not showing alert as another UIAlertController is present already.")
                return
            }
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
    
    /// This function is used to show a toast alert.
    ///
    /// - Parameters:
    ///   - msg: message to be shown on toast
    ///   - controller: controller from where this alert is called
    @objc func showToast(with msg: String, completion: @escaping () -> Void) {
        
        let toast = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        
        let dispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(2)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            toast.dismiss(animated: true, completion: nil)
            completion()
            return
        }
        kMainQueue.async {
            guard let topController = UIApplication.topViewController() else {
                kAppDelegate.window?.rootViewController?.present(toast, animated: true, completion: nil)
                return
            }
            if topController.isKind(of: UIAlertController.self) {
                print("Not showing alert as another UIAlertController is present already.")
                return
            }
            topController.present(toast, animated: true, completion: nil)
        }
    }
    
    /// Show textfield in alert controller
    /// - Parameters:
    ///   - title: Title of the alert
    ///   - message: Brief message for the alert
    ///   - lblDone: Lable string for the done button
    ///   - lblCancel: lable string for the cancel button
    ///   - completion: choice: Bool, _ text: String?
    @objc func showTextfieldAlert(with title: String?, message: String?, placeholder: String, lblDone: String, lblCancel: String, completion: @escaping (_ choice: Bool, _ text: String?) -> Void) {
        let objAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        objAlert.addTextField { textField in
            textField.placeholder = placeholder
        }
        
        let confirmAction = UIAlertAction(title: lblDone, style: .default) { [weak objAlert] _ in
            guard let alertController = objAlert, let textField = alertController.textFields?.first else { return }
            completion(true, textField.text)
        }
        objAlert.addAction(confirmAction)
        
        objAlert.addAction(UIAlertAction(title: lblCancel, style: .destructive, handler: { (_) in
            completion(false, nil)
            return
        }))
        
        kMainQueue.async {
            guard let topController = UIApplication.topViewController() else {
                kAppDelegate.window?.rootViewController?.present(objAlert, animated: true, completion: nil)
                return
            }
            if topController.isKind(of: UIAlertController.self) {
                print("Not showing alert as another UIAlertController is present already.")
                return
            }
            topController.present(objAlert, animated: true, completion: nil)
        }
    }
    
    //MARK:- ------ Get device Info ------
    
    /// This function is used to get the device related information i.e device type, os version, device id.
    ///
    /// - Returns: returns device type, os version, device id.
    func getDeviceInfo() -> [String: String] {
        
        let strModel = UIDevice.current.model //// e.g. @"iPhone", @"iPod touch"
        let strVersion = UIDevice.current.systemVersion // e.g. @"4.0"
        let strDevID = UIDevice.current.identifierForVendor?.uuidString
        
        let tempDict: [String: String] = [
            "device_type": strModel,
            "os_version": strVersion,
            "device_id": strDevID ?? ""
        ]
        return tempDict
    }
    
    
    /// Create the custome table view
    /// - Parameters:
    ///   - tblStyle: Style of the table(.plain or .group)
    ///   - frame: Give the frame to the tableview
    /// - Returns: Table View
    func getTableView(tblStyle: UITableView.Style, frame: CGRect) -> UITableView {
        let tableView: UITableView = UITableView(frame: frame, style: tblStyle)
        //tableView.backgroundColor = UIColor.green
        
        //kTablewViewBgColor
        tableView.sectionIndexBackgroundColor = .clear
        //For iPad separator issue
        tableView.cellLayoutMarginsFollowReadableWidth = false
        tableView.separatorStyle = .none
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        return tableView
    }
    
    
    ///  Check if email string is valid
    /// - Parameter password: password string
    /// - Returns: True if valid else false
    func isValidEmailPassword(_ password: String) -> Bool {
      return try! NSRegularExpression(pattern: "[A-Z]+", options: NSRegularExpression.Options(rawValue: 0)).numberOfMatches(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: password.count)) > 0 && NSRegularExpression(pattern: "[a-z]+", options: NSRegularExpression.Options(rawValue: 0)).numberOfMatches(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: password.count)) > 0 && NSRegularExpression(pattern: "[0-9]+", options: NSRegularExpression.Options(rawValue: 0)).numberOfMatches(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: password.count)) > 0 && password.count >= 8;
    }
    
    
    ///Get percentage
    func percentageValue(attemptQuestion: Int, totalQuestions: Int) -> Float {
        return Float(100*attemptQuestion/totalQuestions) / 100
    }
}

