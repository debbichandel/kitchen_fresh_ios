//
//  EnumUtils.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Device Constraints
extension Screen {
    /// Main Screen
    static let main = UIScreen.main
    /// Screen Width
    static let width = UIScreen.main.bounds.size.width
    ///Screen Height
    static let height = UIScreen.main.bounds.size.height
    /// Screen center width
    static let centerW = Screen.width/2
    /// Screen center height
    static let centerH = Screen.height/2
    /// UserInterfaceIdiom
    static let deviceIdiom = main.traitCollection.userInterfaceIdiom
    /// Is device Ipad?
    static let isIPAD: Bool = deviceIdiom == UIUserInterfaceIdiom.pad ? true : false
}

//
//MARK:-        [---------- STORYBOARD SETTINGS [START] ----------]
//
extension Storyboard {
    
    /// UIStoryboard instance
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    /// This function takes class name as argument and returns it’s instance. Using this, one can instantiate a ViewController as Storyboard.main.viewController(viewControllerClass: ViewController.self)
    ///
    /// - Parameter viewControllerClass: ViewController
    /// - Returns: Storyboard instance for viewControllerClass
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T? {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as? T
    }
}
//MARK:- UIViewController
extension UIViewController {
    
    /// String identifier of storyboard
    class var storyboardID: String {
        return "\(self)"
    }
}

//MARK:
//MARK:        [---------- STORYBOARD SETTINGS [END] ----------]
//MARK:-
/// HTTP METHODS
extension HttpMethods {
    /// GET Method
    static let get = "GET"
    /// POST Method
    static let post = "POST"
}

// MARK: - ContentType
extension ContentType {
    /// application/x-www-form-urlencoded
    static let applicationXWWFormUrlencoded = "application/x-www-form-urlencoded"
    /// application/json
    static let applicationJson = "application/json"
    /// multipart/form-data
    static let multipartFormData = "multipart/form-data"
}

//
//MARK:- Media Extensions
//
/// Media File Extensions
enum MediaExtension: String {
    /// PNG
    case png = "png"
    /// JPG
    case jpg = "jpg"
    /// JPEG
    case jpeg = "jpeg"
    /// MP4
    case mp4 = "mp4"
    /// DOC
    case doc = "doc"
    /// DOCX
    case docX = "docx"
    /// PDF
    case pdf = "pdf"
    /// XLS
    case xls = "xls"
    /// MOV
    case mov = "mov"
    
    /// Name appended with (.)
    var dotName: String {
        return ".\(self.rawValue)"
    }
    
    /// Name
    var name: String {
        return self.rawValue
    }
}
