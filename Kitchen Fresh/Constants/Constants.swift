//
//  Constants.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation
import UIKit

/// AppDelegate shared instance.
// swiftlint:disable:next force_cast
let kAppDelegate = (UIApplication.shared.delegate as! AppDelegate)

/// Main Queue thread.
let kMainQueue = DispatchQueue.main

//MARK:- API
/// Declared further in APIConstants.swift class
///  This enum represent All the APi urls used in the app
extension API {
    /// Main Server Url 'http://runners-planet-dev-api.symple.co.in'
    static let serverURL = "http://192.168.0.9:4433"
    
    /// Base URL for the Api's
    static let baseURL = "\(API.serverURL)/self-service/"
}

//MARK:- FOLDERS

/// FOLDER Names used in the system
enum FOLDERS {
    /// iCloud files folder 'icloud'
    static let icloud = "icloud"
}


/// This enum will represent all the credentials keys for various logins used in the apps.
enum Credentials {
    
}

//MARK:- Storyboard
/// USAGE :
/// - let storyboard = Storyboard.main.instance
/// - let objVC = Storyboard.main.instance.instantiateViewController(withIdentifier: ViewController.storyboardID)
/// - guard let vc = Storyboard.main.viewController(viewControllerClass: ViewController.self) else { return }
/// - Extented further in EnumUtil.swift class
enum Storyboard: String {
    
    ///Practice Storyboard
    case practice = "Practice"
    /// Test Storyboard
    case test = "Test"
    /// More Storyboard
    case more = "More"
}

//MARK:- Background Queues
///QUEUES
enum BackgroundQueue {
    
    /// Queue for login and signUp
    static let loginQueue = DispatchQueue(label: "com.app.queue_signin", attributes: .concurrent)
    
    /// Queue for media download
    static let mediaDownloadQueue = DispatchQueue(label: "com.cudo.queue_media_download", qos: .background)
    
    /// Queue for upload files on Azure Database
    static let filesUploadAzureQueue = DispatchQueue(label: "com.cudo.queue_files_upload", qos: .background)
    
    /// Queue for upload files on Azure Database
    static let filesDownload = DispatchQueue(label: "com.cudo.queue_files_download", qos: .background)
    
    /// Queue for aws keys
    static let commonQueue = DispatchQueue(label: "com.app.queue_common", qos: .background)
}

//MARK:- Device Constraints
///Extented further in EnumUtil.swift class
enum Screen {}

//MARK:- Default Center
///Abbr...
enum DefaultCenter {
    
    /// NotificationCenter
    static let notification = NotificationCenter.default
    
    /// FileManager
    static let fileManager = FileManager.default
    
    /// UserDefaults
    static let userDefaults = UserDefaults.standard
}

/// Common Keys used in Application
enum Keys {
    
    /// Logged In User info
    static let isLogin = "login"
    static let isSubscription = "subscription"
    static let KUserData = "userData"
    static let kToken = "Token"
    static let KUserId = "userId"
}

// MARK:- HTTP METHODS & CONTENT TYPE
///Extented further in EnumUtil.swift class
enum HttpMethods {}

/// Extented further in EnumUtil.swift class
enum ContentType {}

///
// MARK:- Notification Name
///

/// This Enum is used to get the notification name.
enum Notifications: String {
    
    /// dashboardTabBarSelect
    case dashboardTabBarSelect
    
    /// FileSavedStatus
    case fileSavedStatus
    
    /// Name of the notification
    var name: Notification.Name {
        return Notification.Name(rawValue: self.rawValue )
    }
}
