//
//  LocalizedStrings.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation

/// Localization fo used strings
enum L10n {
    
    /// Splash Text
    case splash
    /// Login Btn
    case login
    /// Sign in
    case signIn
    /// Login Title
    case loginTitle
    /// Email Placeholder
    case emailPlaceHolder
    /// Company Placeholder
    case companyPlaceHolder
    /// Password Placeholder
    case passwordPlaceHolder
    /// Forgot Password
    case forgotPassword
    /// Latest Activity
    case latestActivity
    /// Projects
    case project
    case selectProject
    case projects
    case allProjects
    /// Calender
    case calendar
    /// Chat
    case chat
    /// Scan a file
    case scanAFile
    /// Logout
    case logout
    /// Message
    case message
    /// Tasks
    case tasks
    /// Add Task
    case addTask
    /// Files
    case files
    /// Add Files
    case addFiles
    /// Protocols
    case protocols
    /// Messages
    case messages
    /// Email Title
    case emailTitle
    /// Company Title
    case companyTitle
    /// Password Title
    case passwordTitle
    /// Next
    case next
    /// Sync Offline
    case syncOffline
    case lastSync
    case lastSyncedOn
    case spaceOccupied
    case downloadOfflineData
    case syncDownloadOffline
    ///People
    case people
    ///Pending
    case pending
    ///Completed
    case completed
    ///Search
    case searchHere
    ///Save Offline
    case saveOffline
    /// Addtaskfile
    case addTaskFile
    /// TaskTitle
    case taskTitle
    case taskTitlePlaceholder
    /// TaskDescription
    case taskDescription
    case taskDescriptionPlaceHolder
    /// Accociate with work type
    case accociatedWorkType
    case accociatedWorkTypePlaceHolder
    /// Select Phase
    case selectPhase
    /// Select BKP
    case selectBKP
    /// Assignee
    case assignee
    case selectAssignee
    /// Followers
    case followers
    case selectFollowers
    /// Start Date
    case startDate
    /// End Date
    case endDate
    /// Estimated Days
    case estimatedDays
    /// task Configuration
    case taskConfiguration
    /// task Configuration Detail
    case taskConfigurationDetail
    /// continue
    case continueValue
    /// Complete Project
    case completeProject
    /// Clear
    case clear
    /// SelectDate
    case selectDate
    /// SyncTasksOffline
    case syncTasksOffline
    /// SyncFilesOffline
    case syncFilesOffline
    /// SelectAll
    case selectAll
    /// Option
    case option
    /// View Detail
    case viewdetail
    /// Upload New Version
    case uploadNewVersion
    /// Add Task To This File
    case addTaskToThisFile
    /// Download
    case download
    /// Select file source
    case selectFileSource
    /// Load from library
    case loadFromLibrary
    /// Use camera
    case useCamera
    /// Load from files
    case loadFromFiles
    /// No internet connected
    case noInternet
    /// Camera
    case camera
    /// Cancel
    case cancel
    /// OK
    case ok
    /// Dismiss
    case dismiss
    /// Done
    case done
    /// Confirm
    case confirm
    /// Entered email or mobile is not valid
    case enteredEmailOrMobileIsNotValid
    /// Error
    case error
    /// No record found
    case noRecordFound
    /// Password must be 6 characters long
    case passwordMustBe6CharactersLong
    /// Photo Library
    case photoLibrary
    /// Please enter password
    case pleaseEnterPassword
    /// Please enter username
    case pleaseEnterUsername
    /// Saved Photo Album
    case savedPhotoAlbum
    /// Select Source
    case selectSource
    /// Settings
    case settings
    /// Unable to upload file, try again
    case unableToUploadFileTryAgain
    /// Warning
    case warning
    /// You have denied the permission to access camera and gallery
    case youHaveDeniedThePermissionToAccessCamera
    /// "Name can't be blank"
    case noName
    /// "Name should be atleast 2 character long"
    case nameLength
    /// "Email can't be blank"
    case noEmail
    /// "Phone can't be blank"
    case noPhone
    /// "Phone is not valid"
    case noValidPhone
    /// "Email is not valid"
    case invalidEmail
    /// "Password can't be blank"
    case noPwd
    /// "Password minimum 6 char long"
    case pwdLength
    /// "Password and confirm password should be same"
    case pwdNotMatched
    /// "Please choose a profile picture."
    case chooseprofilepic
    /// loader msg
    case loading
    /// Uploading..
    case uploading
    /// Invalid response from server
    case invalidResponse
    /// You are not authorised to login.
    case unauthorised
    /// "Maximum selection limit reached"
    case imgLimitReached
    /// No User is logged in.
    case noLoggedInUser
    /// is Required
    case isRequired
    /// Upload to server
    case uploadtoserver
    /// Authentication Failed
    case authenticationfailed
    /// Please enter 6-digit verification code
    case pleaseEnter6digitVerificationCode
    case pleaseEnterEmail
    case pleaseEnterConfirmPassword
    /// Tab to upload file
    case tabToUploadFile
    /// Upload Files
    case uploadFiles
    /// Generate File Number
    case generateFileNumber
    ///Enable this option to generate file numbering
    case enableOptionGenerateFileNumber
    /// Select
    case select
    /// File Type
    case fileType
    /// File Structure
    case fileStructure
    /// Select People
    case selectpeople
    /// BKP/Folder
    case bkpFolder
    /// Uploaded on
    case uploadedOn
    /// Uploaded By
    case uploadedBy
    /// Comments
    case comments
    /// File Versions
    case fileVersions
    /// add new task
    case addNewTask
    /// click to add comments
    case clickToAddComments
    /// Month
    case month
    /// Week
    case week
    /// UpcomingTask
    case upcomingTask
    /// UpcomingMettings
    case upcomingMetting
    /// Apply
    case apply
    /// WorkType
    case workType
    /// Assign To
    case assignTo
    /// Due Date
    case dueDate
    /// Filter Option
    case filterOption
    /// Update
    case update
    /// Alert
    case alert
    /// Okay
    case okay
    /// PassworValid
    case passwordValid
    /// CheckList Points
    case checkListPoint
    /// Add New
    case addNew
    /// Enter Your Text Here
    case enterYourTextHere
    /// Add Folder
    case addfolder
    /// Folder Name
    case folderName
    /// No Data Found
    case noDataFound
    /// No Data Found Message
    case noDataFoundMessage
    /// Oops
    case oops
    /// Try Again
    case tryAgain
    /// Select Project Validation
    case pleaseSelectProject
    /// Select Phase Validation
    case pleaseSelectPhase
    /// Select Bkp/Folder Validation
    case pleaseSelectBkpFolder
    /// Select File Type Validation
    case pleaseSelectFileType
    /// Select File Structure Validation
    case pleaseSelectFileStructure
    /// Select Peoples Validation
    case pleaseSelectPeoples
    /// Select File Validation
    case pleaseSelectFiles
    /// Yes
    case yes
    /// No
    case no
    /// Logout Title
    case logoutTitle
    /// Error Downloading Image
    case errorDownloadingImage
    
    /// Task Title Validation
    case pleaseEnterTaskTitle
    
    /// Start Date Validation
    case pleaseSelectStartDate
    
    /// End Date Validation
    case pleaseSelectEndDate
    
    /// End Date Validation
    case pleaseSelectEstimationDays
    
    /// Start Date Validation
    case startDateValidation
    
    /// End Date Validation
    case  endDateValidation
}

// swiftlint:enable type_body_length
extension L10n: CustomStringConvertible {
    
    /// Description
    var description: String { return self.string }
    
    /// Actual String value
    var string: String {
        
        switch self {
        case .loadFromFiles:
            return L10n.tr(key: "Load from files")
        case .selectFileSource:
            return L10n.tr(key: "Select file source")
        case .loadFromLibrary:
            return L10n.tr(key: "Load from library")
        case .useCamera:
            return L10n.tr(key: "Use camera")
        case .splash:
            return L10n.tr(key: "The one place for all your projects and team collaboration...!")
        case .login:
            return L10n.tr(key: "LOGIN")
            
        case .signIn:
            return L10n.tr(key: "Sign in")
            
        case .loginTitle:
            return L10n.tr(key: "Enter your login detail to enter inside the application into your account")
            
        case .emailPlaceHolder:
            return L10n.tr(key: "Enter your email address")
            
        case .companyPlaceHolder:
            return L10n.tr(key: "Select Company")
            
        case .passwordPlaceHolder:
            return L10n.tr(key: "Enter your password")
            
        case .forgotPassword:
            return L10n.tr(key: "Forgot Password?")
            
        case .latestActivity:
            return L10n.tr(key: "Latest activity")
            
        case .project:
            return L10n.tr(key: "Project")
            
        case .selectProject:
            return L10n.tr(key: "Select Project")
            
        case .projects:
            return L10n.tr(key: "Projects")
            
        case .allProjects:
            return L10n.tr(key: "All Projects")
            
        case .calendar:
            return L10n.tr(key: "Calendar")
            
        case .chat:
            return L10n.tr(key: "Chat")
            
        case .scanAFile:
            return L10n.tr(key: "Scan a file")
            
        case .logout:
            return L10n.tr(key: "Log Out")
            
        case .message:
            return L10n.tr(key: "Message")
            
        case .tasks:
            return L10n.tr(key: "Tasks")
            
        case .addTask:
            return L10n.tr(key: "Add Task")
            
        case .files:
            return L10n.tr(key: "Files")
            
        case .addFiles:
            return L10n.tr(key: "Add Files")
            
        case .protocols:
            return L10n.tr(key: "Protocols")
            
        case .messages:
            return L10n.tr(key: "Messages")
            
        case .emailTitle:
            return L10n.tr(key: "Enter your login details")
            
        case .companyTitle:
            return L10n.tr(key: "Please select a company")
            
        case .passwordTitle:
            return L10n.tr(key: "Enter your password")
            
        case .next:
            return L10n.tr(key: "NEXT")
            
        case .syncOffline:
            return L10n.tr(key: "Sync Offline Data")
            
        case .downloadOfflineData:
            return L10n.tr(key: "Download offline data")
            
        case .lastSync:
            return L10n.tr(key: "Last Sync")
            
        case .lastSyncedOn:
            return L10n.tr(key: "Last synced on")
            
        case .spaceOccupied:
            return L10n.tr(key: "Space occupied")
            
        case .people:
            return L10n.tr(key: "People")
            
        case .pending:
            return L10n.tr(key: "Pending")
            
        case .completed:
            return L10n.tr(key: "Completed")
            
        case .searchHere:
            return L10n.tr(key: "Search here...")
            
        case .saveOffline:
            return L10n.tr(key: "Save data offline")
            
        case .addTaskFile:
            return L10n.tr(key: "Add task from file")
            
        case .taskTitle:
            return L10n.tr(key: "Task title")
            
        case .taskTitlePlaceholder:
            return L10n.tr(key: "Enter task title")
            
        case .taskDescription:
            return L10n.tr(key: "Description")
            
        case .taskDescriptionPlaceHolder:
            return L10n.tr(key: "Enter Description Here...")
            
        case .accociatedWorkType:
            return L10n.tr(key: "Associate with work type")
            
        case .accociatedWorkTypePlaceHolder:
            return L10n.tr(key: "Select work type")
            
        case .selectPhase:
            return L10n.tr(key: "Select Phase")
            
        case .bkpFolder:
            return L10n.tr(key: "BKP/Folder")
        
        case .selectBKP:
            return L10n.tr(key: "Select BKP")
            
        case .assignee:
            return L10n.tr(key: "Assignee")
            
        case .selectAssignee:
            return L10n.tr(key: "Select assignee")
            
        case .followers:
            return L10n.tr(key: "Followers")
            
        case .selectFollowers:
            return L10n.tr(key: "Select followers")
            
        case .startDate:
            return L10n.tr(key: "Start date")
            
        case .endDate:
            return L10n.tr(key: "End date")
            
        case .estimatedDays:
            return L10n.tr(key: "Estimated days")
            
        case .taskConfiguration:
            return L10n.tr(key: "Task configuration")
            
        case .taskConfigurationDetail:
            return L10n.tr(key: "Send notification to assignee/followers for the task")
            
        case .continueValue:
            return L10n.tr(key: "CONTINUE")
            
        case.completeProject:
            return L10n.tr(key: "Complete project")
            
        case .clear:
            return L10n.tr(key: "Clear")
            
        case .syncDownloadOffline:
            return L10n.tr(key: "Sync/Download Offline Now")
            
        case .selectDate:
            return L10n.tr(key: "Select date")
            
        case .syncTasksOffline:
            return L10n.tr(key: "Sync tasks offline")
            
        case .syncFilesOffline:
            return L10n.tr(key: "Sync files offline")
            
        case .selectAll:
            return L10n.tr(key: "Select All")
            
        case .option:
            return L10n.tr(key: "Option")
            
        case .viewdetail:
            return L10n.tr(key: "View detail")
            
        case .uploadNewVersion:
            return L10n.tr(key: "Upload new version")
            
        case .addTaskToThisFile:
            return L10n.tr(key: "Add task to this file")
            
        case .download:
            return L10n.tr(key: "Download")
            
        case .pleaseEnter6digitVerificationCode:
            return L10n.tr(key: "Please enter 6-digit verification code")
        case .authenticationfailed:
            return L10n.tr(key: "Authentication Failed")
        case .noInternet:
            return L10n.tr(key: "There is no internet connection.\nPlease check your connectivity")
        case .camera:
            return L10n.tr(key: "Camera")
        case .cancel:
            return L10n.tr(key: "Cancel")
        case .ok:
            return L10n.tr(key: "OK")
        case .dismiss:
            return L10n.tr(key: "Dismiss")
        case .done:
            return L10n.tr(key: "Done")
        case .confirm:
            return L10n.tr(key: "Confirm")
        case .enteredEmailOrMobileIsNotValid:
            return L10n.tr(key: "Entered email or mobile is not valid")
        case .error:
            return L10n.tr(key: "Error")
        case .noRecordFound:
            return L10n.tr(key: "No record found")
        case .passwordMustBe6CharactersLong:
            return L10n.tr(key: "Password must be 6 characters long")
        case .photoLibrary:
            return L10n.tr(key: "Photo Library")
        case .pleaseEnterConfirmPassword:
            return L10n.tr(key: "Please enter confirm password")
        case .pleaseEnterPassword:
            return L10n.tr(key: "Please enter password")
        case .pleaseEnterUsername:
            return L10n.tr(key: "Please enter username")
        case .pleaseEnterEmail:
            return L10n.tr(key: "Please enter email")
        case .savedPhotoAlbum:
            return L10n.tr(key: "Saved Photo Album")
        case .selectSource:
            return L10n.tr(key: "Select Source")
        case .settings:
            return L10n.tr(key: "Settings")
        case .unableToUploadFileTryAgain:
            return L10n.tr(key: "Unable to upload file, try again")
        case .warning:
            return L10n.tr(key: "Warning")
        case .youHaveDeniedThePermissionToAccessCamera:
            return L10n.tr(key: "You have denied the permission to access camera and gallery")
        case .noName:
            return L10n.tr(key: "User name can't be blank")
        case .nameLength:
            return L10n.tr(key: "Minimum 2 characters required")
        case .noEmail:
            return L10n.tr(key: "Email can't be blank")
        case .invalidEmail:
            return L10n.tr(key: "Email is not valid")
        case .noPhone:
            return L10n.tr(key: "Phone can't be blank")
        case .noValidPhone:
            return L10n.tr(key: "Phone is not valid")
        case .noPwd:
            return L10n.tr(key: "Password can't be blank")
        case .pwdLength:
            return L10n.tr(key: "Minimum 6 characters required")
        case .pwdNotMatched:
            return L10n.tr(key: "Password & Confirm password not matched")
        case .chooseprofilepic:
            return L10n.tr(key: "Please choose a profile picture.")
        case .loading:
            return L10n.tr(key: "Loading..")
        case .uploading:
            return L10n.tr(key: "Uploading")
        case .invalidResponse:
            return L10n.tr(key: "Invalid response from server")
        case .unauthorised:
            return L10n.tr(key: "You are not authorised to login.")
        case .imgLimitReached:
            return L10n.tr(key: "Maximum selection limit reached")
        case .noLoggedInUser:
            return L10n.tr(key: "No User is Logged In")
        case .isRequired:
            return L10n.tr(key: "is required")
        case .uploadtoserver:
            return L10n.tr(key: "Upload to server as:")
        case .tabToUploadFile:
            return L10n.tr(key: "Tap to upload file")
        case .uploadFiles:
            return L10n.tr(key: "Upload files")
        case .generateFileNumber:
            return L10n.tr(key: "Generate file number")
        case.enableOptionGenerateFileNumber:
            return L10n.tr(key: "Enable this option to generate file numbering")
        case.select:
            return L10n.tr(key: "Select")
        case.fileType:
            return L10n.tr(key: "File Type")
        case.fileStructure:
            return L10n.tr(key: "File Structure")
        case.selectpeople:
            return L10n.tr(key: "Select People")
        case .uploadedOn:
            return L10n.tr(key: "Uploaded On")
        case .uploadedBy:
            return L10n.tr(key: "Uploaded by")
        case .comments:
            return L10n.tr(key: "Comments")
        case .fileVersions:
            return L10n.tr(key: "File versions")
        case .addNewTask:
            return L10n.tr(key: "Add new task")
        case .clickToAddComments:
            return L10n.tr(key: "Click to add comment")
        case .month:
            return L10n.tr(key: "Monthly")
        case .week:
            return L10n.tr(key: "Weekly")
        case .upcomingTask:
            return L10n.tr(key: "Upcoming tasks")
        case .upcomingMetting:
            return L10n.tr(key: "Upcoming meetings")
        case .apply:
            return L10n.tr(key: "Apply")
        case .workType:
            return L10n.tr(key: "WorkType")
        case .assignTo:
            return L10n.tr(key: "Assign to")
        case .dueDate:
            return L10n.tr(key: "Due date")
        case .filterOption:
            return L10n.tr(key: "Filter option")
        case .update:
            return L10n.tr(key: "Update")
        case .alert:
            return L10n.tr(key: "Alert")
        case .okay:
            return L10n.tr(key: "Okay")
        case .passwordValid:
            return L10n.tr(key: "Password must contain at least 8 digits, one uppercase, one lowercase, one number")
        case .checkListPoint:
            return L10n.tr(key: "Checklist points")
        case .addNew:
            return L10n.tr(key: "Add new")
        case .enterYourTextHere:
           return L10n.tr(key: "Enter your text here")
        case .addfolder:
            return L10n.tr(key: "Add folder")
        case .folderName:
            return L10n.tr(key: "Folder Name")
        case .noDataFound:
            return L10n.tr(key: "No data found")
        case .noDataFoundMessage:
            return L10n.tr(key: "We are not able to see any update here")
        case .oops:
            return L10n.tr(key: "Oops")
        case .tryAgain:
            return L10n.tr(key: "Try Again")
        case .pleaseSelectProject:
            return L10n.tr(key: "Please select project")
        case .pleaseSelectPhase:
            return L10n.tr(key: "Please select Phase")
        case .pleaseSelectBkpFolder:
            return L10n.tr(key: "Please select BKP/Folder")
        case .pleaseSelectFileType:
            return L10n.tr(key: "Please select File Type")
        case .pleaseSelectFileStructure:
            return L10n.tr(key: "Please select File Structure")
        case .pleaseSelectPeoples:
            return L10n.tr(key: "Please select people")
        case .pleaseSelectFiles:
            return L10n.tr(key: "Please select file")
        case .yes:
            return L10n.tr(key: "Yes")
        case .no:
            return L10n.tr(key: "No")
        case .logoutTitle:
            return L10n.tr(key: "Are you sure you want to log out?")
        case .errorDownloadingImage:
            return L10n.tr(key: "Error in downloading image")
        case .pleaseEnterTaskTitle:
            return L10n.tr(key: "Please enter task title")
        case .pleaseSelectStartDate:
            return L10n.tr(key: "Please select start date")
        case .pleaseSelectEndDate:
            return L10n.tr(key: "Please select end date")
        case .pleaseSelectEstimationDays:
           return L10n.tr(key: "Please add estimation days")
        case .startDateValidation:
           return L10n.tr(key: "StartDate should not be greater then end date")
        case .endDateValidation:
           return L10n.tr(key: "EndDate should not be less then start date")
        }
        
    }
    
    private static func tr(key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

/// Value for localised key
/// - Parameter key: String
func tr(_ key: L10n) -> String {
    return key.string
}

private final class BundleToken {}
