//
//  Colors.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation
import UIKit

///
///USAGE
///

/// Create an enum to handle each and every colour required in our application.
enum Color {
    /// Shadow Color
    static let shadowColor = Color.shadow.value
    
    /// Shadow Color with Alpha
    static let UIColorUIColorUIColor = Color.shadow.withAlpha(0.5)
    
    /// Custom Color with Alpha
    static let customColorWithAlpha = Color.custom(hexString: "#123edd", alpha: 0.25).value
    
    /// Instagram Color
    static let instaColor = Color.custom(hexString: "#DE5153", alpha: 1.0).value
    
    /// Facebook Color
    static let fbColor = Color.custom(hexString: "#2D4486", alpha: 1.0).value
    
    /// TextField Background Color
    static let textFieldBGColor = Color.custom(hexString: "#1b1b1b", alpha: 1.0).value
    
    /// Green Color for progress bar
    static let progressGreenColor = Color.custom(hexString: "#00d169", alpha: 1.0).value
    
    /// Navigation Color
    static let navigationBarColor = UIColor.init(hexString: "#FFFFFF")
    
    /// TabBar Color
    static let tabBarBorderColor = UIColor.init(hexString: "#505C6229")
    
    /// Side Menu
    static let sideMenuTitleColor = UIColor.init(hexString: "#1B1B40")
    
    /// Side menu selected title color
    static let sideMenuSelectedTitleColor = UIColor.init(hexString: "#2D62ED")
    
    /// Search textfield title color
    static let searchFieldColor = UIColor.init(hexString: "#6E8CA0")
    
    ///Practice
    static let wrongQuestionTitleColor = UIColor.init(hexString: "#C70202")
    static let wrongQuestionColor = UIColor.init(hexString: "#FF4D4D")
    static let rightQuestionTitleColor = UIColor.init(hexString: "#1F7800")
    static let rightQuestionColor = UIColor.init(hexString: "#77B762")
    static let explanationColor = UIColor.init(hexString: "#EEF3FD")
    
    ///Tasks
    static let topBorderColor = UIColor.init(hexString: "#2D62ED")
    static let taskComplete = UIColor.init(hexString: "#EFFFF7")
    
    ///Calender
    static let upcomingMeetingColor = UIColor.init(hexString: "#FF9057")
    static let upcomingTaskColor = UIColor.init(hexString: "#2D62ED")
    
    /// Empty Table View
    static let emptyTableViewColor  = UIColor.init(hexString: "#9FA2A5")
    
    /// No internet Connecton
    static let noInternetConnection = UIColor.init(hexString: "#F6F8FC")
    
    /// Files
    static let bkpSelectedRowColor = UIColor.init(hexString: "#F4F5F6")
    
    ///Question
    static let questionColor = UIColor.init(hexString: "#E5E5EA")
    
    ////
    /// CASE STARTS FROM HERE
    
    ///
    ///Colours on Navigation Bar, Button Titles, Progress Indicator etc.
    case theme
    
    ///Hair line separators in between views.
    case border
    
    /// Shadow colours for card like design.
    case shadow
    
    /// Dark background colour to group UI components with light colour.
    case darkBackground
    
    /// Light background colour to group UI components with dark colour.
    case lightBackground
    
    /// Used for grouping UI elements with some other colour scheme.
    case intermidiateBackground
    
    /// Dark Text Colour
    case darkText
    
    /// Light Text Colour
    case lightText
    
    /// Intermediate Text Colour
    case intermidiateText
    
    /// Colour to show success, something right for user.
    case affirmation
    
    /// Colour to show error, some danger zones for user.
    case negation
    
    /// custom(hexString: String, alpha: Double) to get UIColor values other than the previous ones.
    case custom(hexString: String, alpha: Double)
    
    /// withAlpha(_ alpha: Double) to get UIColor values with opacity.
    func withAlpha(_ alpha: Double) -> UIColor {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }
}

/// Put the values (hex string or RGB literal) to the following extension of Color enum
extension Color {
    
    /// Color from hex
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
        case .border:
            instanceColor = UIColor(hexString: "#3c3c3c")
        case .theme:
            instanceColor = UIColor(hexString: "#212121")
        case .shadow:
            instanceColor = UIColor(hexString: "#cccccc")
        case .darkBackground:
            instanceColor = UIColor(hexString: "#1b1b1b")
        case .lightBackground:
            instanceColor = UIColor(hexString: "#D0D8DF")
        case .intermidiateBackground:
            instanceColor = UIColor(hexString: "#cccc99")
        case .darkText:
            instanceColor = UIColor(hexString: "#333333")
        case .intermidiateText:
            instanceColor = UIColor(hexString: "#999999")
        case .lightText:
            instanceColor = UIColor(hexString: "#d8d8d8")
        case .affirmation:
            instanceColor = UIColor(hexString: "#00ff66")
        case .negation:
            instanceColor = UIColor(hexString: "#ff3300")
        case .custom(let hexValue, let opacity):
            instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        }
        return instanceColor
    }
}
