//
//  AppDelegate.swift
//  Cudo
//
//  Created by softobiz on 09/02/21.
//

import UIKit
import Reachability
import IQKeyboardManagerSwift
import SVProgressHUD

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    /// Default UIWindow instance.
    var window: UIWindow?
    
    /// Reachability instance to check network Reachability.
    let reachability = try? Reachability()
    
    /// Override point for customization after application launch.
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        ///---------------------------------------------------------
        ///Enable Keyboard Manager
        ///
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //---------------------------------------------------------
        
        
        ///SVProgressHUD Make Center
        ///window = UIWindow(frame: UIScreen.main.bounds)
        
        
        /// -------------------------------------------------------
        /// Set SVProgressHUB Loader Color
        ///
        SVProgressHUD.setForegroundColor(Color.topBorderColor)    //Ring Color
        /// -------------------------------------------------------
        
        /// -----------------------------------------
        /// Setup navBar.....
        ///
        UINavigationBar.appearance().barTintColor = Color.navigationBarColor
        /// -----------------------------------------
        
        /// -----------------------------------------
        /// Remove local stored media files
        ///
        removeLocalStoredFiles()
        /// -----------------------------------------
        
        ///
        /// OPT OUT DARK MODE
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            self.window?.overrideUserInterfaceStyle = .light
        }
        
        
//        var homeVC = UIViewController()
//        homeVC = ActivityTabBar()
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        self.window?.rootViewController = homeVC
//        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    /// App terminate
    /// Add the test taken check if the app terminate by the user so user will be loss the test
    func applicationWillTerminate(_ application: UIApplication) {
        print("Terminated")
    }
    
    /// Set Orientation Mode(Portrait)
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
}
