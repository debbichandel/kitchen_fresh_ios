//
//  AppDelegate+Helpers.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation
import SVProgressHUD
import UIKit

//MARK:- LOADERS

/// COMMON HELERS
extension AppDelegate {
    /// This function is used to show the spinning loader on view and hide the loader from the view.
    ///
    /// - Parameters:
    ///   - title: This will be written under the spinner.
    ///   - isOn: This will disable the user interaction to app if true.
    func showLoader(with title: String = L10n.loading.string, interaction isOn: Bool = false) {
        kMainQueue.async {
            if isOn == false {
                self.screenInteraction(enable: false)
            }
            SVProgressHUD.show(withStatus: title)
        }
    }
    
    /// Show loader with progress bar
    /// - Parameters:
    ///   - progress: Float
    ///   - status: String
    ///   - isOn: This will disable the user interaction to app if false.
    func showProgressLoader(progress: Float, status: String, interaction isOn: Bool = false) {
        kMainQueue.async {
            if isOn == false {
                self.screenInteraction(enable: false)
            }
            SVProgressHUD.showProgress(progress, status: status)
        }
    }
    
    /// This will replace the title under the spinning loader.
    ///
    /// - Parameter title: New title to replace with.
    func replaceLoader(title: String = "") {
        kMainQueue.async {
            SVProgressHUD.setStatus(title)
        }
    }
    
    /// Hide the spinning loader
    func hideLoader() {
        let seconds = 0.2
        kMainQueue.asyncAfter(deadline: .now() + seconds) {
            self.screenInteraction(enable: true)
            SVProgressHUD.dismiss()
        }
    }
    
    /// This will enable/disable the screen interactions during any specific event.
    ///
    /// - Parameter enable: If passed True then interaction will start otherwise if passed False then interaction will be stopped.
    func screenInteraction(enable: Bool) {
        if enable == true {
            if UIApplication.shared.isIgnoringInteractionEvents == true { //If already ignoring the screen touch's
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        } else {
            if UIApplication.shared.isIgnoringInteractionEvents == false { //If not already ignoring the screen touch's
                UIApplication.shared.beginIgnoringInteractionEvents()
            }
        }
    }
}


//MARK:- INTERNET
extension AppDelegate {
    /// Used to check the internet is available or not. Return bool to controll further processings.
    ///
    /// - Parameter toast: weather to show toast if not connected or not.
    /// - Returns: return true if internet is connected else return false.
    func checkInternet(with toast: Bool = true) -> Bool {
        guard let connection = reachability?.connection else {
            return false
        }
        
        switch connection {
        case .cellular:
            print("internet connected: cellular")
            return true
        case .none, .unavailable:
            if toast == true {
                kMainQueue.async {
                    ASUtility.shared.showToast(with: L10n.noInternet.string, completion: {})
                }
            }
            print("internet not connected: None")
            return false
        case .wifi:
            print("internet connected: wifi")
            return true
        }
    }
    
    
    /// Start monitoring the internet continueusly.
    func internetMonitoring() {
        DefaultCenter.notification.addObserver(self, selector: #selector(checkReachability(notification:)), name: NSNotification.Name.reachabilityChanged, object: nil)
        
        do {
            try reachability?.startNotifier()
        } catch let err {
            print("+++++++++++ ERROR IN INTERNET +++++++++++++++++")
            print(err.localizedDescription)
        }
    }
    
    /// This will be called whenever network reachability changed
    ///
    /// - Parameter notification: NSNotification of the observer.
    @objc func checkReachability(notification: NSNotification) {
        
        if checkInternet(with: false) == true {
            print("INTERNET Connected")
            Singleton.shared.isOffline = false
            
            /// Save Files To Server
            
            /// Reload the Files Screen Data
            DefaultCenter.notification.post(name: Notifications.fileSavedStatus.name, object: nil, userInfo: [:])
            
            
        } else {
            print("INTERNET DIS-Connected")
            Singleton.shared.isOffline = true
            
            /// Reload the Files Screen Data
            DefaultCenter.notification.post(name: Notifications.fileSavedStatus.name, object: nil, userInfo: [:])
        }
    }
}

//MARK:- FILEMANAGER
extension AppDelegate {
    
    /// Remove All files stored in document directory of mov exension
    @objc func removeLocalStoredFiles() {
        guard let dirUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        ///
        /// Uncomment below line to remove files with particular extensions
        ///
        //        let arrExts = ["mov", "m4v", "mp4", "pdf"]
        //        arrExts.forEach { (ext) in
        //            FileManager.removeAllItemsInsideDirectory(at: dirUrl, with: ext)
        //            FileManager.removeAllItemsInsideDirectory(at: dirUrl, with: ext.uppercased())
        //        }
        ///
        /// Remove iCloud folder files as well
        let iCloudDirUrl = dirUrl.appendingPathComponent(FOLDERS.icloud, isDirectory: true)
        FileManager.removeAllItemsInsideDirectory(atPath: iCloudDirUrl.path)
    }
    
}

