//
//  Singleton.swift
//  Runners Planet
//
//  Created by softobiz on 18/09/20.
//  Copyright © 2020 softobiz. All rights reserved.
//

import Foundation

/// This is a singleton class to store data during app cycle
class Singleton: NSObject {
    
    /// Shared instance
    static let shared = Singleton()
    
    /// Subscription
    var isSubscription: Bool = false
    
    /// Internet connection
    var isOffline: Bool = false
}
